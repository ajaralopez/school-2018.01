import collections
import glob
import os
import pickle

from ...core.document_tokenizer import DocumentTokenizer
from .str_storage import StringStorage
from ..logger import LOGGER


class InvertedIndexBuilder:
    def __init__(self, config):
        self._base_dir = config["base_dir"]
        self._inv_index = collections.defaultdict(list)
        self._config = config
        self._storage = StringStorage()

    def _process_document(self, document):
        LOGGER.info("Proccessing document: %s" % document)
        counts = collections.Counter(document.tokens)

        for key in counts:
            if len(self._inv_index[key]) < self._config["max_doc_count"]:
                self._inv_index[key].append((counts[key], self._storage.get_index(document.name)))
            else:
                min_element = min(self._inv_index[key])
                index = self._inv_index[key].index(min_element)
                self._inv_index[index] = (counts[key], document.name)

    @staticmethod
    def _scan_py(dir):
        '''
        scan_all -> list
        dir - Directory to scan
        Return exmaple:["2.py","path/x.py","path/dir/s.py","path/dir/duck.py"]
        '''
        return glob.glob(os.path.join(dir, "**/*.py"), recursive=True)

    def _save(self, name):
        LOGGER.info("Compressing python file: %s" % name)
        with open(name + ".index", "wb+") as f:
            pickle.dump(self._inv_index, f)
        with open(name + ".strings", "wb+") as f:
            pickle.dump(self._storage, f)

    def build(self):
        '''
        Builds index and saves it to index/inverted.index
        :return: None
        '''

        for file in self._scan_py(os.path.join(self._base_dir, "repos/")):
            doc = DocumentTokenizer(file)
            self._process_document(doc)

        self._save(os.path.join(self._base_dir, "index/inverted"))
